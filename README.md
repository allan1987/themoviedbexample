# The Movie DB Example #

Este projeto utilizou a API de filmes TheMovieDB (https://www.themoviedb.org) como base para exibição de uma lista de filmes, assim como seus respectivos detalhes.
Foi implementado usando o modelo MVP (Model View Presenter).

### Tecnologias ###
* Dagger 2 (injeção de dependências)
* Retrofit 2 (interação com a API de terceiros)
* RXJava 2 (comunicação reativa)
* ButterKnife (injeção de views)
* Glide (gerenciamento de imagens)
* ViewModel (Arch components)
* LyfeCycle (arch components)
* Mockito (testes unitários)
* Java 8
* RecyclerView
* CardView