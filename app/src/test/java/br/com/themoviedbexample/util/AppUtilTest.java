package br.com.themoviedbexample.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by allan on 26/11/17.
 */

public class AppUtilTest {

    @Test
    public void testIsNullOrEmptyWithNullString() throws Exception {
        Assert.assertTrue(AppUtil.isNullOrEmpty((String)null));
    }

    @Test
    public void testIsNullOrEmptyWithEmptyString() throws Exception {
        Assert.assertTrue(AppUtil.isNullOrEmpty(""));
    }

    @Test
    public void testIsNullOrEmptyWithNonNullString() throws Exception {
        Assert.assertFalse(AppUtil.isNullOrEmpty("test"));
    }

    @Test
    public void testIsNullOrEmptyWithNullList() throws Exception {
        Assert.assertTrue(AppUtil.isNullOrEmpty((List)null));
    }

    @Test
    public void testIsNullOrEmptyWithEmptyList() throws Exception {
        Assert.assertTrue(AppUtil.isNullOrEmpty(new ArrayList()));
    }
}
