package br.com.themoviedbexample.util;

import android.content.Context;
import android.widget.ImageView;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by allan on 26/11/17.
 */

public class GlideUtilTest {

    @Mock
    Context context;

    @Mock
    ImageView imageView;

    @Test
    public void testLoadImageWithNullPath() throws Exception {
        Assert.assertNull(GlideUtil.loadImage(context, null, imageView));
        Assert.assertNull(GlideUtil.loadImage(context, null, imageView, null, null));
        Assert.assertNull(GlideUtil.loadImageWithFadeIn(context, null, imageView, null));
    }
}
