package br.com.themoviedbexample.details;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

import br.com.themoviedbexample.core.lifecycle.LifecycleStrategy;
import br.com.themoviedbexample.list.ListContract;
import br.com.themoviedbexample.list.ListPresenter;
import br.com.themoviedbexample.model.MovieDetails;
import br.com.themoviedbexample.model.MoviesResponse;
import br.com.themoviedbexample.service.RestClient;
import br.com.themoviedbexample.util.AppUtil;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by allan on 26/11/17.
 */

public class DetailsPresenterTest {

    @Mock
    private DetailsContract.View view;

    @Mock
    private RestClient restClient;

    @Mock
    private LifecycleStrategy strategy;

    private DetailsPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new DetailsPresenter(view, restClient, strategy, Schedulers.trampoline());
    }

    @Test
    public void testGetMovieDetailsSuccess() throws Exception {
        MovieDetails movieDetails = loadJSONFromFile();
        Flowable<MovieDetails> data = Flowable.just(movieDetails);

        Mockito.when(restClient.getMovieDetails(Matchers.anyLong())).thenReturn(data);

        presenter.getMovieDetails(123L);

        Mockito.verify(view).hideMessage();
        Mockito.verify(view).showLoading();
        Mockito.verify(restClient).getMovieDetails(123L);

        Mockito.verify(view).hideLoading();
        Mockito.verify(view).fillDetails(movieDetails);
        Mockito.verify(view).saveDetails(movieDetails);
        Mockito.verify(view, Mockito.never()).showError(Matchers.anyString());
    }

    @Test
    public void testGetMovieDetailsError() throws Exception {
        Mockito.when(restClient.getMovieDetails(Matchers.anyLong())).thenReturn(null);

        presenter.getMovieDetails(123L);

        Mockito.verify(view).hideMessage();
        Mockito.verify(view).showLoading();
        Mockito.verify(restClient).getMovieDetails(123L);

        Mockito.verify(view).hideLoading();
        Mockito.verify(view).showError(Matchers.anyString());
        Mockito.verify(view).saveDetails(null);
        Mockito.verify(view, Mockito.never()).fillDetails(Matchers.any(MovieDetails.class));

    }

    private MovieDetails loadJSONFromFile() throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(AppUtil.getBasePath()
                + "test/resources/movieDetails.json"));
        return new Gson().fromJson(br, MovieDetails.class);
    }
}
