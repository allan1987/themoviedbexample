package br.com.themoviedbexample.list;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

import br.com.themoviedbexample.core.lifecycle.LifecycleStrategy;
import br.com.themoviedbexample.model.MoviesResponse;
import br.com.themoviedbexample.service.RestClient;
import br.com.themoviedbexample.util.AppUtil;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by allan on 25/11/17.
 */

public class ListPresenterTest {

    @Mock
    private ListContract.View view;

    @Mock
    private RestClient restClient;

    @Mock
    private LifecycleStrategy strategy;

    private ListPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new ListPresenter(view, restClient, strategy, Schedulers.trampoline());
    }

    @Test
    public void testLoadMoviesWithWrongPage() throws Exception {
        MoviesResponse moviesResponse = loadJSONFromFile();
        Flowable<MoviesResponse> data = Flowable.just(moviesResponse);

        Mockito.when(restClient.loadNowPlayingMovies(Matchers.anyInt())).thenReturn(data);

        presenter.loadMovies(0);

        Mockito.verify(view).hideMessage();
        Mockito.verify(view).showLoading();
        Mockito.verify(restClient).loadNowPlayingMovies(1);

        Mockito.verify(view).hideLoading();
        Mockito.verify(view).fillMovies(moviesResponse.getResults());
        Mockito.verify(view).saveMovies(moviesResponse.getResults());
        Mockito.verify(view, Mockito.never()).showEmpty();
        Mockito.verify(view, Mockito.never()).showError(Matchers.anyString());
    }

    @Test
    public void testLoadMoviesWithNullResponse() throws Exception {
        Mockito.when(restClient.loadNowPlayingMovies(Matchers.anyInt())).thenReturn(null);

        presenter.loadMovies(1);

        Mockito.verify(view).hideMessage();
        Mockito.verify(view).showLoading();
        Mockito.verify(restClient).loadNowPlayingMovies(1);

        Mockito.verify(view).hideLoading();
        Mockito.verify(view).showError(Matchers.anyString());
        Mockito.verify(view).saveMovies(null);
        Mockito.verify(view, Mockito.never()).showEmpty();
        Mockito.verify(view, Mockito.never()).fillMovies(Matchers.any(List.class));
    }

    @Test
    public void testLoadMoviesWithoutResults() throws Exception {
        MoviesResponse moviesResponse = loadJSONFromFile();
        moviesResponse.setResults(null);
        Flowable<MoviesResponse> data = Flowable.just(moviesResponse);

        Mockito.when(restClient.loadNowPlayingMovies(Matchers.anyInt())).thenReturn(data);

        presenter.loadMovies(1);

        Mockito.verify(view).hideMessage();
        Mockito.verify(view).showLoading();
        Mockito.verify(restClient).loadNowPlayingMovies(1);

        Mockito.verify(view).hideLoading();
        Mockito.verify(view).showEmpty();
        Mockito.verify(view).saveMovies(moviesResponse.getResults());
        Mockito.verify(view, Mockito.never()).fillMovies(moviesResponse.getResults());
        Mockito.verify(view, Mockito.never()).showError(Matchers.anyString());
    }

    private MoviesResponse loadJSONFromFile() throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(AppUtil.getBasePath()
                + "test/resources/moviesResponse.json"));
        return new Gson().fromJson(br, MoviesResponse.class);
    }
}
