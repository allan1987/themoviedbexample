package br.com.themoviedbexample.util;

import java.io.File;
import java.util.List;

/**
 * Created by allan on 25/11/17.
 */

public class AppUtil {

    public static boolean isNullOrEmpty(List list) {
        return list == null || list.size() == 0;
    }

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static String getBasePath() {
        String basePath = "./src/";
        File file = new File(basePath);
        if (!file.exists()) basePath = "./app/src/";
        return basePath;
    }
}
