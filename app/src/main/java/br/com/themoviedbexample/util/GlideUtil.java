package br.com.themoviedbexample.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import br.com.themoviedbexample.BuildConfig;
import br.com.themoviedbexample.R;

/**
 * Created by allan on 25/11/17.
 */

public class GlideUtil {

    public static Target<Drawable> loadImageWithFadeIn(Context context, String path, ImageView imageView,
                                                       RequestListener<Drawable> requestListener) {
        return loadImage(context, path, imageView, GenericTransitionOptions.with(android.R.anim.fade_in),
                requestListener);
    }

    public static Target<Drawable> loadImage(Context context, String path, ImageView imageView) {
        return loadImage(context, path, imageView, null, null);
    }

    public static Target<Drawable> loadImage(Context context, String path, ImageView imageView,
                                             TransitionOptions<?,Drawable> transitionOptions,
                                             RequestListener<Drawable> requestListener) {
        if (AppUtil.isNullOrEmpty(path)) return null;
        RequestBuilder<Drawable> requestBuilder = Glide.with(context)
                .load(BuildConfig.MOVIE_DB_IMAGE_API_URL+ path);

        if (requestListener != null) requestBuilder.listener(requestListener);

        if (transitionOptions != null) requestBuilder.transition(transitionOptions);

        return requestBuilder.into(imageView);
    }
}
