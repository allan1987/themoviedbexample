package br.com.themoviedbexample.core.lifecycle;

import android.arch.lifecycle.LifecycleOwner;

import io.reactivex.disposables.Disposable;

/**
 * Created by allan on 23/11/17.
 */

public class LifecycleStrategy {

    private DisposeStrategy strategy;

    public LifecycleStrategy(LifecycleOwner owner, DisposeStrategy strategy) {
        this.strategy = strategy;
        owner.getLifecycle().addObserver(strategy);
    }

    public void applyStrategy(Disposable toDispose) {
        strategy.addDisposable(toDispose);
    }
}
