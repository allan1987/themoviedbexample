package br.com.themoviedbexample.core;

import android.content.Context;

import java.util.List;

import br.com.themoviedbexample.model.Movie;

/**
 * Created by allan on 23/11/17.
 */

public interface BaseContract {

    interface View {
        void showLoading();
        void hideLoading();
        void showError(String message);
        void hideMessage();
        Context getApplicationContext();
    }

    interface Interaction {

    }
}
