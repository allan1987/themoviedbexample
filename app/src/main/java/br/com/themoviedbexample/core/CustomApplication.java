package br.com.themoviedbexample.core;

import android.app.Activity;
import android.app.Application;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import br.com.themoviedbexample.dagger.component.DaggerAppComponent;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by allan on 23/11/17.
 */

public class CustomApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> injector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent.builder().application(this).build().inject(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.get(this).onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Glide.get(this).onTrimMemory(level);
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return injector;
    }
}
