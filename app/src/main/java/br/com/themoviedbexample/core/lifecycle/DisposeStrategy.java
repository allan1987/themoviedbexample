package br.com.themoviedbexample.core.lifecycle;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by allan on 23/11/17.
 */

public class DisposeStrategy implements LifecycleObserver {

    private CompositeDisposable composite = new CompositeDisposable();

    public void addDisposable(Disposable toDispose) {
        composite.add(toDispose);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        composite.clear();
    }
}
