package br.com.themoviedbexample.core;

import br.com.themoviedbexample.core.lifecycle.LifecycleStrategy;
import br.com.themoviedbexample.service.RestClient;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

/**
 * Created by allan on 23/11/17.
 */

public abstract class BasePresenter<V extends BaseContract.View, T> {

    protected V view;
    protected RestClient restClient;
    protected LifecycleStrategy strategy;
    private Scheduler scheduler;

    public BasePresenter(V view, RestClient restClient, LifecycleStrategy strategy, Scheduler uiScheduler) {
        this.view = view;
        this.restClient = restClient;
        this.strategy = strategy;
        this.scheduler = uiScheduler;
    }

    protected void execute(Flowable<T> dataFlow) {
        if (dataFlow == null) {
            handleResponseError(new Throwable("no response was found"));
            return;
        }

        Disposable toDispose = dataFlow.observeOn(scheduler).subscribe(
                this::handleResponse,
                this::handleResponseError
        );

        strategy.applyStrategy(toDispose);
    }

    protected abstract void handleResponse(T response);
    protected abstract void handleResponseError(Throwable throwable);
}
