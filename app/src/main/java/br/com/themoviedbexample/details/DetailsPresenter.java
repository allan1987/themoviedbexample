package br.com.themoviedbexample.details;

import br.com.themoviedbexample.R;
import br.com.themoviedbexample.core.BasePresenter;
import br.com.themoviedbexample.core.lifecycle.LifecycleStrategy;
import br.com.themoviedbexample.model.MovieDetails;
import br.com.themoviedbexample.service.RestClient;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by allan on 25/11/17.
 */

public class DetailsPresenter extends BasePresenter<DetailsContract.View, MovieDetails>
        implements DetailsContract.Interaction {

    public DetailsPresenter(DetailsContract.View view, RestClient restClient, LifecycleStrategy strategy,
                            Scheduler uiScheduler) {
        super(view, restClient, strategy, uiScheduler);
    }

    @Override
    public void getMovieDetails(Long id) {
        view.hideMessage();
        view.showLoading();
        execute(restClient.getMovieDetails(id));
    }

    @Override
    protected void handleResponse(MovieDetails response) {
        view.hideLoading();
        view.fillDetails(response);
        view.saveDetails(response);
    }

    @Override
    protected void handleResponseError(Throwable throwable) {
        view.hideLoading();
        view.showError(throwable.getLocalizedMessage());
        view.saveDetails(null);
    }
}