package br.com.themoviedbexample.details;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;

import javax.inject.Inject;

import br.com.themoviedbexample.R;
import br.com.themoviedbexample.model.MovieDetails;
import br.com.themoviedbexample.util.GlideUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

/**
 * Created by allan on 25/11/17.
 */

public class DetailsActivity extends AppCompatActivity implements DetailsContract.View {

    public static final String EXTRA_MOVIE_ID = "movie_id";

    @BindView(R.id.error_message)
    protected TextView errorMessage;

    @BindView(R.id.progress_bar)
    protected ProgressBar loading;

    @BindView(R.id.title_text)
    protected TextView movieTitleText;

    @BindView(R.id.overview_text)
    protected TextView overviewText;

    @BindView(R.id.release_date_text)
    protected TextView releaseDateText;

    @BindView(R.id.vote_average_text)
    protected TextView voteAverageText;

    @BindView(R.id.backdrop_image)
    protected ImageView backdropImage;

    @Inject
    protected DetailsPresenter presenter;

    private DetailsViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);

        init();
    }

    private void init() {
        setTitle(R.string.details_activity_title);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        model = ViewModelProviders.of(this).get(DetailsViewModel.class);
        model.setPresenter(presenter);
        if (model.hasMovieDetails()) {
            fillDetails(model.getMovieDetails());
        } else if (getIntent().hasExtra(EXTRA_MOVIE_ID)) {
            model.loadMovieDetails(getIntent().getLongExtra(EXTRA_MOVIE_ID, 0));
        }
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void showError(String string) {
        errorMessage.setText(string);
        errorMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMessage() {
        errorMessage.setVisibility(View.GONE);
    }

    @Override
    public void fillDetails(MovieDetails details) {
        GlideUtil.loadImageWithFadeIn(this, details.getBackdropPath(), backdropImage,
                new RequestListener<Drawable>() {
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model,
                                                   Target<Drawable> target, DataSource dataSource,
                                                   boolean isFirstResource) {
                        fillMovieDetails(details);
                        return false;
                    }

                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        backdropImage.setVisibility(View.GONE);
                        fillMovieDetails(details);
                        return false;
                    }
                });
    }

    private void fillMovieDetails(MovieDetails details) {
        movieTitleText.setText(details.getTitle());
        releaseDateText.setText(getString(R.string.release_date_text, details.getReleaseDate()));

        DecimalFormat dF = new DecimalFormat("#.##");
        String vote = getString(R.string.average_vote_text, dF.format(details.getVoteAverage()));

        voteAverageText.setText(vote);

        overviewText.setText(details.getOverview());
    }

    @Override
    public void saveDetails(MovieDetails details) {
        model.setMovieDetails(details);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
