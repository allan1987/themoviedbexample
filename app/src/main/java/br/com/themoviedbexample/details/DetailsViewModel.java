package br.com.themoviedbexample.details;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import br.com.themoviedbexample.model.MovieDetails;

/**
 * Created by allan on 25/11/17.
 */

public class DetailsViewModel extends ViewModel {

    private MutableLiveData<MovieDetails> details = new MutableLiveData<>();

    private DetailsPresenter presenter;

    public void setPresenter(DetailsPresenter presenter) {
        this.presenter = presenter;
    }

    public LiveData<MovieDetails> loadMovieDetails(Long id) {
        if (presenter != null) presenter.getMovieDetails(id);
        return details;
    }

    public MovieDetails getMovieDetails() {
        return details.getValue();
    }

    public void setMovieDetails(MovieDetails movieDetails) {
        details.setValue(movieDetails);
    }

    public boolean hasMovieDetails() {
        return details.getValue() != null;
    }
}
