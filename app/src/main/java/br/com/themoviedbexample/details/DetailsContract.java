package br.com.themoviedbexample.details;

import br.com.themoviedbexample.core.BaseContract;
import br.com.themoviedbexample.model.MovieDetails;

/**
 * Created by allan on 25/11/17.
 */

public interface DetailsContract {

    interface View extends BaseContract.View {
        void fillDetails(MovieDetails response);
        void saveDetails(MovieDetails response);
    }

    interface Interaction extends BaseContract.Interaction {
        void getMovieDetails(Long id);
    }
}
