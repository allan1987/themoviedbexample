package br.com.themoviedbexample.service;

/**
 * Created by allan on 25/11/17.
 */

public class NetworkError extends RuntimeException {

    public NetworkError(String message) {
        super(message);
    }
}
