package br.com.themoviedbexample.service;

import br.com.themoviedbexample.model.MovieDetails;
import br.com.themoviedbexample.model.MoviesResponse;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;

/**
 * Created by allan on 23/11/17.
 */

public class RestClient {

    private CustomService service;
    private Scheduler scheduler;

    public RestClient(CustomService service, Scheduler ioScheduler) {
        this.service = service;
        scheduler = ioScheduler;
    }

    public Flowable<MoviesResponse> loadNowPlayingMovies(int page) {
        return service.loadNowPlayingMovies(page)
                .subscribeOn(scheduler)
                .compose(new NetworkErrorHandler<>());
    }

    public Flowable<MovieDetails> getMovieDetails(Long id) {
        return service
                .getMovieDetails(id)
                .subscribeOn(scheduler)
                .compose(new NetworkErrorHandler<>());
    }
}
