package br.com.themoviedbexample.service;

import br.com.themoviedbexample.BuildConfig;
import br.com.themoviedbexample.model.MovieDetails;
import br.com.themoviedbexample.model.MoviesResponse;
import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by allan on 23/11/17.
 */

public interface CustomService {

    @GET("4/discover/movie?with_release_type=2|3&api_key=" + BuildConfig.MOVIE_DB_API_KEY)
    Flowable<MoviesResponse> loadNowPlayingMovies(@Query("page") int page);

    @GET("3/movie/{id}?api_key=" + BuildConfig.MOVIE_DB_API_KEY)
    Flowable<MovieDetails> getMovieDetails(@Path("id") Long id);
}
