package br.com.themoviedbexample.list;

import java.util.List;

import br.com.themoviedbexample.core.BaseContract;
import br.com.themoviedbexample.model.Movie;

/**
 * Created by allan on 23/11/17.
 */

public interface ListContract {

    interface View extends BaseContract.View {
        void showEmpty();
        void fillMovies(List<Movie> results);
        void saveMovies(List<Movie> results);
    }

    interface Interaction extends BaseContract.Interaction {
        void loadMovies(int page);
    }
}
