package br.com.themoviedbexample.list;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import br.com.themoviedbexample.R;
import br.com.themoviedbexample.details.DetailsActivity;
import br.com.themoviedbexample.model.Movie;
import br.com.themoviedbexample.util.AppUtil;
import br.com.themoviedbexample.util.GlideUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by allan on 25/11/17.
 */

public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Movie> movies = new ArrayList<>();

    private Activity activity;

    public ListAdapter(Activity activity) {
        this.activity = activity;
    }

    public void add(Movie movie) {
        int pos = movies.size();
        movies.add(movie);
        notifyItemInserted(pos);
    }

    public void addAll(List<Movie> movies) {
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    public void clear() {
        movies.clear();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View card = LayoutInflater.from(activity)
                .inflate(R.layout.view_movie_item, parent, false);
        return new MovieHolder(card);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Movie movie = movies.get(position);

        MovieHolder viewHolder = (MovieHolder) holder;
        GlideUtil.loadImage(activity, movie.getPosterPath(), viewHolder.posterImage);
        viewHolder.ratingBar.setRating(movie.getVoteAverage() / 2);

        viewHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(activity, DetailsActivity.class);
            intent.putExtra(DetailsActivity.EXTRA_MOVIE_ID, movie.getId());
            activity.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return !AppUtil.isNullOrEmpty(movies) ? movies.size() : 0;
    }

    public static class MovieHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.movie_item_poster_image)
        public ImageView posterImage;

        @BindView(R.id.movie_item_rating_bar)
        public AppCompatRatingBar ratingBar;

        public MovieHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public List<Movie> getItems() {
        return movies;
    }
}
