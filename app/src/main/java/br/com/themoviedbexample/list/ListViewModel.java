package br.com.themoviedbexample.list;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import br.com.themoviedbexample.model.Movie;
import br.com.themoviedbexample.util.AppUtil;

/**
 * Created by allan on 25/11/17.
 */

public class ListViewModel extends ViewModel {

    private MutableLiveData<List<Movie>> mMovies = new MutableLiveData<>();

    private ListPresenter mPresenter;

    public void setPresenter(ListPresenter presenter) {
        mPresenter = presenter;
    }

    public LiveData<List<Movie>> loadMovies() {
        return loadMovies(1);
    }

    public LiveData<List<Movie>> loadMovies(int currentPage) {
        if (mPresenter != null) mPresenter.loadMovies(currentPage);
        return mMovies;
    }

    public List<Movie> getMovies() {
        return mMovies.getValue();
    }

    public void setMovies(List<Movie> movies) {
        mMovies.setValue(movies);
    }

    public boolean hasMovies() {
        return !AppUtil.isNullOrEmpty(mMovies.getValue());
    }
}
