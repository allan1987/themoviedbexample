package br.com.themoviedbexample.list;

import br.com.themoviedbexample.core.BasePresenter;
import br.com.themoviedbexample.core.lifecycle.LifecycleStrategy;
import br.com.themoviedbexample.model.MoviesResponse;
import br.com.themoviedbexample.service.RestClient;
import br.com.themoviedbexample.util.AppUtil;
import io.reactivex.Scheduler;

/**
 * Created by allan on 23/11/17.
 */

public class ListPresenter extends BasePresenter<ListContract.View, MoviesResponse>
        implements ListContract.Interaction {

    public ListPresenter(ListContract.View view, RestClient restClient, LifecycleStrategy strategy,
                         Scheduler uiScheduler) {
        super(view, restClient, strategy, uiScheduler);
    }

    @Override
    public void loadMovies(int page) {
        if (page <= 0) page = 1;
        view.hideMessage();
        view.showLoading();
        execute(restClient.loadNowPlayingMovies(page));
    }

    @Override
    protected void handleResponse(MoviesResponse response) {
        view.hideLoading();
        if (AppUtil.isNullOrEmpty(response.getResults())) view.showEmpty();
        else view.fillMovies(response.getResults());
        view.saveMovies(response.getResults());
    }

    @Override
    protected void handleResponseError(Throwable throwable) {
        view.hideLoading();
        view.showError(throwable.getLocalizedMessage());
        view.saveMovies(null);
    }
}
