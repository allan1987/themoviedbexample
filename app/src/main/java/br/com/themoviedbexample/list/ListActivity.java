package br.com.themoviedbexample.list;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import br.com.themoviedbexample.R;
import br.com.themoviedbexample.model.Movie;
import br.com.themoviedbexample.util.EndlessRecyclerScrollListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

/**
 * Created by allan on 23/11/17.
 */

public class ListActivity extends AppCompatActivity implements ListContract.View {

    @BindView(R.id.swipe_refresh)
    protected SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.recyclerview_movies)
    protected RecyclerView moviesView;

    @BindView(R.id.container)
    protected View container;

    @BindView(R.id.error_message)
    protected TextView errorMessage;

    @BindView(R.id.progress_bar)
    protected ProgressBar loading;

    @Inject
    protected ListPresenter presenter;

    private ListViewModel model;

    private ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ButterKnife.bind(this);

        init();
    }

    private void init() {
        setTitle(R.string.main_activity_title);

        hideMessage();

        adapter = new ListAdapter(this);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        moviesView.addOnScrollListener(new EndlessRecyclerScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                model.loadMovies(currentPage);
            }
        });

        moviesView.setLayoutManager(layoutManager);
        moviesView.setAdapter(adapter);

        swipeRefresh.setOnRefreshListener(() -> {
            adapter.clear();
            model.loadMovies();
        });

        model = ViewModelProviders.of(this).get(ListViewModel.class);
        model.setPresenter(presenter);

        if (model.hasMovies()) adapter.addAll(model.getMovies());
        else model.loadMovies();
    }

    @Override
    public void fillMovies(List<Movie> results) {
        adapter.addAll(results);
    }

    @Override
    public void saveMovies(List<Movie> results) {
        model.setMovies(results);
    }

    @Override
    public void showLoading() {
        if (!swipeRefresh.isRefreshing()) {
            swipeRefresh.setEnabled(false);
            loading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
        if (!swipeRefresh.isEnabled()) swipeRefresh.setEnabled(true);
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void showEmpty() {
        errorMessage.setVisibility(View.VISIBLE);
        errorMessage.setText(R.string.empty);
    }

    @Override
    public void showError(String message) {
        errorMessage.setVisibility(View.VISIBLE);
        errorMessage.setText(message);
    }

    @Override
    public void hideMessage() {
        errorMessage.setVisibility(View.GONE);
    }
}
