package br.com.themoviedbexample.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by allan on 23/11/17.
 */

public class MoviesResponse implements Serializable {

    private List<Movie> results;

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
