package br.com.themoviedbexample.model;

/**
 * Created by allan on 25/11/17.
 */

public class ListItem {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
