package br.com.themoviedbexample.dagger.module;

import br.com.themoviedbexample.core.lifecycle.LifecycleStrategy;
import br.com.themoviedbexample.dagger.qualifier.SchedulerUI;
import br.com.themoviedbexample.details.DetailsContract;
import br.com.themoviedbexample.details.DetailsPresenter;
import br.com.themoviedbexample.list.ListContract;
import br.com.themoviedbexample.list.ListPresenter;
import br.com.themoviedbexample.service.RestClient;
import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by allan on 23/11/17.
 */

@Module
public class PresentationModule {

    @Provides
    static ListPresenter listPresenter(RestClient restClient, ListContract.View view,
                                       LifecycleStrategy strategy, @SchedulerUI Scheduler uiScheduler) {
        return new ListPresenter(view, restClient, strategy, uiScheduler);
    }

    @Provides
    static DetailsPresenter detailsPresenter(RestClient restClient, DetailsContract.View view,
                                          LifecycleStrategy strategy, @SchedulerUI Scheduler uiScheduler) {
        return new DetailsPresenter(view, restClient, strategy, uiScheduler);
    }
}