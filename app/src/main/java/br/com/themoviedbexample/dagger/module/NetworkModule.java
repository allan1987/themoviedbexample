package br.com.themoviedbexample.dagger.module;

import javax.inject.Singleton;

import br.com.themoviedbexample.BuildConfig;
import br.com.themoviedbexample.dagger.qualifier.SchedulerIO;
import br.com.themoviedbexample.service.CustomService;
import br.com.themoviedbexample.service.RestClient;
import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by allan on 23/11/17.
 */

@Module
public class NetworkModule {

    @Provides
    @Singleton
    static RestClient restClient(CustomService service, @SchedulerIO Scheduler ioScheduler) {
        return new RestClient(service, ioScheduler);
    }

    @Provides
    @Singleton
    static CustomService service(OkHttpClient httpClient) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(BuildConfig.MOVIE_DB_API_URL)
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return adapter.create(CustomService.class);
    }

    @Provides
    @Singleton
    static OkHttpClient httpClient(Interceptor interceptor) {
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    @Provides
    @Singleton
    static Interceptor interceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }
}