package br.com.themoviedbexample.dagger.component;

import android.app.Application;

import javax.inject.Singleton;

import br.com.themoviedbexample.core.CustomApplication;
import br.com.themoviedbexample.dagger.module.ActivityBuilder;
import br.com.themoviedbexample.dagger.module.AppModule;
import br.com.themoviedbexample.dagger.module.NetworkModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by allan on 23/11/17.
 */

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class,
        NetworkModule.class, ActivityBuilder.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }

    void inject(CustomApplication application);
}
