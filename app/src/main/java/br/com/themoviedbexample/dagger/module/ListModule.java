package br.com.themoviedbexample.dagger.module;

import android.arch.lifecycle.LifecycleOwner;

import br.com.themoviedbexample.list.ListActivity;
import br.com.themoviedbexample.list.ListContract;
import dagger.Module;
import dagger.Provides;

/**
 * Created by allan on 23/11/17.
 */

@Module(includes = {LifecycleStrategyModule.class, PresentationModule.class})
public class ListModule {

    @Provides
    static ListContract.View listView(ListActivity activity) {
        return activity;
    }

    @Provides
    static LifecycleOwner strategist(ListActivity activity) {
        return activity;
    }
}
