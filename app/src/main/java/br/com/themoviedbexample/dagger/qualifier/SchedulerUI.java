package br.com.themoviedbexample.dagger.qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by allan on 23/11/17.
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface SchedulerUI {}
