package br.com.themoviedbexample.dagger.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import br.com.themoviedbexample.dagger.qualifier.SchedulerIO;
import br.com.themoviedbexample.dagger.qualifier.SchedulerUI;
import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by allan on 23/11/17.
 */

@Module
public class AppModule {

    @Provides
    @Singleton
    static Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    @SchedulerUI
    static Scheduler uiScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    @SchedulerIO
    static Scheduler ioScheduler() {
        return Schedulers.io();
    }
}
