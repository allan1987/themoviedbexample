package br.com.themoviedbexample.dagger.module;

import android.arch.lifecycle.LifecycleOwner;

import br.com.themoviedbexample.core.lifecycle.DisposeStrategy;
import br.com.themoviedbexample.core.lifecycle.LifecycleStrategy;
import dagger.Module;
import dagger.Provides;

/**
 * Created by allan on 23/11/17.
 */

@Module
public class LifecycleStrategyModule {

    @Provides
    static LifecycleStrategy strategy(LifecycleOwner owner) {
        return new LifecycleStrategy(owner, new DisposeStrategy());
    }
}
