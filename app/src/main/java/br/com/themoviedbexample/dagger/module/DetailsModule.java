package br.com.themoviedbexample.dagger.module;

import android.arch.lifecycle.LifecycleOwner;

import br.com.themoviedbexample.details.DetailsActivity;
import br.com.themoviedbexample.details.DetailsContract;
import br.com.themoviedbexample.list.ListActivity;
import br.com.themoviedbexample.list.ListContract;
import dagger.Module;
import dagger.Provides;

/**
 * Created by allan on 23/11/17.
 */

@Module(includes = {LifecycleStrategyModule.class, PresentationModule.class})
public class DetailsModule {

    @Provides
    static DetailsContract.View detailsView(DetailsActivity activity) {
        return activity;
    }

    @Provides
    static LifecycleOwner strategist(DetailsActivity activity) {
        return activity;
    }
}
