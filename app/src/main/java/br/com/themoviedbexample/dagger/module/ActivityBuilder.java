package br.com.themoviedbexample.dagger.module;

import br.com.themoviedbexample.details.DetailsActivity;
import br.com.themoviedbexample.list.ListActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by allan on 23/11/17.
 */

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = ListModule.class)
    abstract ListActivity listActivity();

    @ContributesAndroidInjector(modules = DetailsModule.class)
    abstract DetailsActivity detailsActivity();
}
